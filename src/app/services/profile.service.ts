import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { forkJoin } from "rxjs/observable/forkJoin";

@Injectable()
export class ProfileService {
    private username: string;
    private clientid: string = 'b228dee06972d3f71993';
    private clientsecret: string = '412c497e079e43c02b15c8d8828b56e8f7354b22';

    constructor(private http: Http) { 
console.log("service is now ready!");
        this.username = 'rushint';
    }

    setProfile(username:string) {
        this.username = username;
    }

    getProfileInfo() {
        return this.http.get("https://api.github.com/users/" + this.username + "?client_id=" + this.clientid + "&client_secret=" + this.clientsecret)
            .map(res => res.json());
    }

    getProfileRepos() {
        return this.http.get("https://api.github.com/users/" + this.username + "/repos?client_id=" + this.clientid + "&client_secret=" + this.clientsecret)
        .map(res => res.json());
    }

    getProfileGists() {
        return this.http.get("https://api.github.com/users/" + this.username + "/gists?client_id=" + this.clientid + "&client_secret=" + this.clientsecret)
        .map(res => res.json());
    }

    getProfileAll() {
        let info = this.http.get("https://api.github.com/users/" + this.username + "?client_id=" + this.clientid + "&client_secret=" + this.clientsecret);
        let repos = this.http.get("https://api.github.com/users/" + this.username + "/repos?client_id=" + this.clientid + "&client_secret=" + this.clientsecret);
        let gists = this.http.get("https://api.github.com/users/" + this.username + "/gists?client_id=" + this.clientid + "&client_secret=" + this.clientsecret);

        return forkJoin([info, repos, gists]).map(res => res);
    }
}