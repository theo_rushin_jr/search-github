import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../services/profile.service';

import { LoaderService } from '../../services/loader.service';

@Component({
    selector: 'sg-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
    profile: any[];
    repos: any[];
    gists: any[];
    username: string;

    constructor(private profileService: ProfileService, private loaderService: LoaderService) { 

    }

    findProfile(ev){
        if(ev.keyCode == 13) {
            this.loaderService.display(true);

            this.profileService.setProfile(this.username);

            this.profileService.getProfileAll().subscribe(results => {
console.log('results: ', results);
                this.profile = results[0].json();

                this.repos = results[1].json();

                this.gists = results[2].json();

                this.loaderService.display(false);
            });
        }
    }

    ngOnInit() {

    }
}